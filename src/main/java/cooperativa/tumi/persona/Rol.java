package cooperativa.tumi.persona;

public class Rol {
	private Integer idRol;
	private String descripcion;
	private Integer estado;
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		return "Rol [descripcion=" + descripcion + ", estado=" + estado + "]";
	}
	
}
