package cooperativa.tumi.servicio;

public class Estado {
	private Integer idEstado;
	private Integer idCuenta;
	public Integer getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}
	public Integer getIdCuenta() {
		return idCuenta;
	}
	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}
	@Override
	public String toString() {
		return "Estado [idEstado=" + idEstado + ", idCuenta=" + idCuenta + "]";
	}
	
}
