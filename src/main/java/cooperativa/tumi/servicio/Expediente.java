package cooperativa.tumi.servicio;

public class Expediente {
	private Integer idPersona;
	private Integer idCuenta;
	private Integer idExpedinte;
	private Integer idExpedienteDetalle;
	
	public Integer getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}
	public Integer getIdCuenta() {
		return idCuenta;
	}
	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}
	public Integer getIdExpedinte() {
		return idExpedinte;
	}
	public void setIdExpedinte(Integer idExpedinte) {
		this.idExpedinte = idExpedinte;
	}
	public Integer getIdExpedienteDetalle() {
		return idExpedienteDetalle;
	}
	public void setIdExpedienteDetalle(Integer idExpedienteDetalle) {
		this.idExpedienteDetalle = idExpedienteDetalle;
	}
	
	@Override
	public String toString() {
		return "Expediente [idPersona=" + idPersona + ", idCuenta=" + idCuenta + ", idExpedinte=" + idExpedinte
				+ ", idExpedienteDetalle=" + idExpedienteDetalle + "]";
	}	
	
}
